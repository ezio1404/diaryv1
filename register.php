<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>MyDiary</title>
  <link href="view/assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="view/assets/css/custom.style.css" rel="stylesheet">
  <link href="view/assets/css/custom.css" rel="stylesheet">
  <link href="view/assets/fonts/css/font-awesome.min.css" rel="stylesheet" >
</head>
<body>
  <div id="login">
    <div class="container  ">
        <h3 class="text-center text-primary pt-5"></h3>
      <div id="login-row" class="row justify-content-center align-items-center custom-width">
        <div id="login-column" class="col-md-6">
          <div id="login-box " class="col-md-12">
            <h3 class="text-center text-info">Register</h3>
            <form class="needs-validation" novalidate action="controller/owner/owner.controller.php" method="POST">
              <div class="form-row">
                <div class="col-md-5 mb-3">
                  <label for="validationCustom01">First name</label>
                  <input type="text" class="form-control" id="validationCustom01" name="first_name" placeholder="First name" 
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-5 mb-3">
                  <label for="validationCustom02">Last name</label>
                  <input type="text" class="form-control" id="validationCustom02" name="last_name" placeholder="Last name"
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-2 mb-3">
                  <label for="validationCustom021">M.I</label>
                  <input type="text" class="form-control" id="validationCustom021" name="middle_initial" placeholder="M.I"
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 mb-3">
                  <label for="validationCustomUsername">Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend">@</span>
                    </div>
                    <input type="text" class="form-control" id="validationCustomUsername" name="username" placeholder="Username"
                      aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                      Please choose a username.
                    </div>
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="validationCustomPassword">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock"  aria-hidden="true"></i></span>
                    </div>
                    <input type="password" class="form-control" id="validationCustomPassword" name="password" placeholder="Password"
                      aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                      Please choose a password.
                    </div>
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <label for="recheckValidationCustomPassword">Confirm Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    </div>
                    <input type="password" class="form-control" id="recheckValidationCustomPassword" placeholder="Password"
                      aria-describedby="inputGroupPrepend" name="confirm_password" id="recheckValidationCustomPassword" required>
                    </div>
                    <label  id="message"></label>
                  </div>
            
              </div>
              <div class="form-group">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                  <label class="form-check-label" for="invalidCheck">
                    Agree to terms and conditions
                  </label>
                  <div class="invalid-feedback">
                    You must agree before submitting.
                  </div>
                </div>
              </div>
              <input class="btn btn-primary" type="submit" value="Register" name="addOwner">
              <a class="btn btn-danger"  href="index.php">Cancel</a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="view/assets/js/jquery.min.js"></script>
<script src="view/assets/js/bootstrap.min.js"></script>
<script src="view/assets/js/validate.js"></script>
<script src="view/assets/js/passwordValidation.js"></script>
</body>

</html>