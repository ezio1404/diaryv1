-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2018 at 08:41 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diary_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_diary`
--

CREATE TABLE `tbl_diary` (
  `diary_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `diary_datecreated` date NOT NULL,
  `diary_label` varchar(255) NOT NULL,
  `diary_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_diary`
--

INSERT INTO `tbl_diary` (`diary_id`, `owner_id`, `diary_datecreated`, `diary_label`, `diary_status`) VALUES
(6, 3, '2018-09-19', 'asd', 1),
(7, 4, '2018-09-21', 'asdas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_like`
--

CREATE TABLE `tbl_like` (
  `like_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `story_id` int(11) NOT NULL,
  `like_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_like`
--

INSERT INTO `tbl_like` (`like_id`, `owner_id`, `story_id`, `like_status`) VALUES
(1, 3, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_owner`
--

CREATE TABLE `tbl_owner` (
  `owner_id` int(11) NOT NULL,
  `owner_lastname` varchar(255) NOT NULL,
  `owner_firstname` varchar(255) NOT NULL,
  `owner_mi` varchar(1) NOT NULL,
  `owner_username` varchar(255) NOT NULL,
  `owner_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_owner`
--

INSERT INTO `tbl_owner` (`owner_id`, `owner_lastname`, `owner_firstname`, `owner_mi`, `owner_username`, `owner_password`) VALUES
(3, 'test', 'test', 't', 'test', 'test'),
(4, 'test2', 'test2', 't', 'test2', 'test2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_story`
--

CREATE TABLE `tbl_story` (
  `story_id` int(11) NOT NULL,
  `diary_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `story_title` varchar(255) NOT NULL,
  `story_date` date NOT NULL,
  `story_content` text NOT NULL,
  `story_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_story`
--

INSERT INTO `tbl_story` (`story_id`, `diary_id`, `owner_id`, `story_title`, `story_date`, `story_content`, `story_status`) VALUES
(30, 6, 3, 'asdasd', '2018-09-19', 'testtesttesttesttesttesttesttesttesttesttesttest', 0),
(31, 6, 3, 'asdasdasdasd', '2018-09-13', 'asdasd', 1),
(32, 7, 4, 'asdasdasd', '2018-09-21', 'asdasdasdasdasdasdasdasdasdas', 0),
(33, 7, 4, 'dasdasdas', '2018-09-21', 'asdasdasasdasd', 0),
(34, 7, 4, 'sdasdasd', '2018-09-21', 'asda', 0),
(36, 6, 3, 'anyeong sayo', '2018-09-21', 'testing shits', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_diary`
--
ALTER TABLE `tbl_diary`
  ADD PRIMARY KEY (`diary_id`),
  ADD UNIQUE KEY `diary_label` (`diary_label`);

--
-- Indexes for table `tbl_like`
--
ALTER TABLE `tbl_like`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `tbl_owner`
--
ALTER TABLE `tbl_owner`
  ADD PRIMARY KEY (`owner_id`);

--
-- Indexes for table `tbl_story`
--
ALTER TABLE `tbl_story`
  ADD PRIMARY KEY (`story_id`),
  ADD KEY `diary_id` (`diary_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_diary`
--
ALTER TABLE `tbl_diary`
  MODIFY `diary_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_like`
--
ALTER TABLE `tbl_like`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_owner`
--
ALTER TABLE `tbl_owner`
  MODIFY `owner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_story`
--
ALTER TABLE `tbl_story`
  MODIFY `story_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_story`
--
ALTER TABLE `tbl_story`
  ADD CONSTRAINT `tbl_story_ibfk_1` FOREIGN KEY (`diary_id`) REFERENCES `tbl_diary` (`diary_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
