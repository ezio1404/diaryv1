<?php
    require_once 'db/dbhelper.php';

    Class Like extends DBHelper{
        private $table = "tbl_like";
        private $fields = array(
            "owner_id",
            "story_id",
            "like_status"
        );
        private $fieldsUpdate = array(
            "like_status"
        );
        //constructor
    function __construct(){
        return DBHelper::__construct();
    }
    // Create
    function addLike($data){
        return DBHelper::insertRecord($data,$this->fields,$this->table); 
    }
    // Retreive
    function getAllLike(){
        return DBHelper::getAllRecord($this->table);
    }
    function getLikeById($ref_id){
        return DBHelper::getRecordById($this->table,'like_id',$ref_id);
    }
    function getLike($ref_id){
        return DBHelper::getRecord($this->table,'like_id',$ref_id);
    }

    // Update
    function updateLike($data,$ref_id){
        // $sql="SELECT like_status FROM tbl_like as l , tbl_owner as o,tbl_story as s WHERE l.owner_id=o.".$owner_id." AND l.story_id=s.'.$story_id";
        // SELECT like_status FROM tbl_like as l , tbl_owner as o,tbl_story as s WHERE l.owner_id=o.owner_id AND s.story_id=l.story_id
        $sql= "UPDATE tbl_like SET like_status =? WHERE owner_id=? AND story_id=?";

        return DBHelper::updateRecord($this->table,$this->fieldsUpdate,$data,'owner_id','story_id','like_id',$ref_id); 
    }
    function updateStatus($data,$ref_id){
        return DBHelper::updateRecord($this->table,'like_status',$data,'owner_id',$ref_id); 
    }
    // Delete
    function deleteLike($ref_id){
            return DBHelper::deleteRecord($this->table,'like_id',$ref_id);
    }


    }
?>