<?php
    require_once 'db/dbhelper.php';

    Class Story extends DBHelper{
        private $table = "tbl_story";
        private $fields = array(
            "diary_id",
            "owner_id",
            "story_title",
            "story_date",
            "story_content"
        );
        private $fieldsUpdate = array(
            "diary_id",
            "owner_id",
            "story_title",
            "story_date",
            "story_content",
            "story_status"
        );
        //constructor
    function __construct(){
        return DBHelper::__construct();
    }
    // Create
    function addStory($data){
        return DBHelper::insertRecord($data,$this->fields,$this->table); 
    }
    // Retreive
    function getAllStory(){
        return DBHelper::getAllRecord($this->table);
    }
    function getStoryById($ref_id){
        return DBHelper::getRecordById($this->table,'story_id',$ref_id);
    }
    function getStory($ref_id){
        return DBHelper::getRecord($this->table,'story_id',$ref_id);
    }
    function getAllStoryByDiary($ref_id){
        return DBHelper::getRecord($this->table,'diary_id',$ref_id);
    }
    // Update
    function updateStory($data,$ref_id){
        return DBHelper::updateRecord($this->table,$this->fieldsUpdate,$data,'story_id',$ref_id); 
    }
    // Delete
    function deleteStory($ref_id){
            return DBHelper::deleteRecord($this->table,'story_id',$ref_id);
    }
    
    //some functions
    function filterStory($id,$firstValue,$secondValue){
        $table =$this->table.' as s, tbl_owner as o';
        $condition = 's.owner_id = o.owner_id';
        $field= 's.story_date';
        return DBHelper::searchBetween($id,$table,$field,'s.owner_id',$firstValue,$secondValue,$condition);
    }

    }
?>