<?php
    require '../../model/log.model.php';
    $log = new Log();
    if(isset($_POST['login'])){
        $username=htmlentities($_POST['username']);
        $password=htmlentities($_POST['password']);
        $data=array($username,$password);
        $ok=$log->login($data);
  
        if($ok){
            header("location:../../view/index.php?id=".$_SESSION['owner_id']."&?info=".$_SESSION['owner']);
        }else{
            header("location:../../index.php?failed_login");
        }
    }
    if(isset($_POST['logout'])){
        unset($_SESSION['owner_id']);
        unset($_SESSION['owner']);
        session_destroy();
        header("location:../../index.php?success_logout");
    }