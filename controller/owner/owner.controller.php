<?php
    require '../../model/owner.model.php';
    $owner=new Owner();
    if(isset($_POST['addOwner']))
    {
        $flag = true;
        $last=$_POST['last_name'];
        $first=$_POST['first_name'];
        $mi=$_POST['middle_initial'];
        $username=$_POST['username'];
        $password=$_POST['password'];
        $ownerArray = array($last,$first,$mi,$username,$password);

        for($i=0;$i<count($ownerArray);$i++){
            if($ownerArray[$i] == ""){
                $flag = false;
                break;
            }
        }
        
        if($flag){
            $owner->addOwner($ownerArray);
            header('location:../../index.php?Succes_adding');
        }
        else{
            $message = "Invalid Credentials";
        }
    }
    
?>