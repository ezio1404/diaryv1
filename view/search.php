<?php
require_once '../model/owner.model.php';

if ($_SESSION) {
$Owner=new Owner();
$row=$Owner->getOwnerById($_SESSION['owner_id']);
$diaries=$Owner->searchDiary(array("test"));
  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MyDiary</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/custom.style.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
<a class="navbar-brand" href="index.php"><i class="fa fa-book" aria-hidden="true"></i>MyDiary</a>
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $row['owner_lastname'].",".$row['owner_firstname']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="settings.php">Settings</a>
          <div class="dropdown-divider"></div>
           <form action="../controller/owner/owner.log.php" method="post">
            <input class="dropdown-item" type="submit" value="Logout" name="logout">
          </form>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="diary.php">Diary</a>
      </li>
    </ul>
  </div>
</nav>

  <?php
foreach($diaries as $dia){
?>
  <div class="custom-width">
    <div class="container custom-padding">
    <div class="inline">
        <?php
          if($dia['diary_status']==1){
        ?>
        <a href="story.php?id=<?php  echo $dia['diary_id']?>?&owner_id=<?php  echo $dia['owner_id']?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <?php
          }
?>
      </div>
      <h1 class="display-4">
        <?php  echo $dia['diary_label']?>
      </h1>
      <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
      <?php echo $dia['diary_datecreated']?>
    </div>
  </div>
  <?php
}
?>




</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<?php

} else {
  header("Location:../index.php?Please_login");
}
?>
</html>
