<?php
require_once '../model/owner.model.php';

if ($_SESSION) {
$Owner=new Owner();
$row=$Owner->getOwnerById($_SESSION['owner_id']);

  ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MyDiary</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/custom.style.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><i class="fa fa-book" aria-hidden="true"></i>MyDiary</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $row['owner_lastname'].",".$row['owner_firstname']?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="settings.php">Settings</a>
          <div class="dropdown-divider"></div>
           <form action="../controller/owner/owner.log.php" method="post">
            <input class="dropdown-item" type="submit" value="Logout" name="logout">
          </form>
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="diary.php">Diary</a>
      </li>
    </ul>

  </div>
</nav>


  <div class="custom-width">
    <div class="container custom-padding">

      <h1 class="display-4">
        Update Account
      </h1>
      <form class="needs-validation" novalidate action="controller/owner/owner.controller.php" method="POST">
              <div class="form-row">
                <div class="col-md-5 mb-3">
                  <label for="validationCustom01">First name</label>
                  <input type="text" class="form-control" id="validationCustom01" name="first_name" placeholder="First name"  value="<?php echo $row['owner_firstname']?>"
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-5 mb-3">
                  <label for="validationCustom02">Last name</label>
                  <input type="text" class="form-control" id="validationCustom02" name="last_name" placeholder="Last name"value="<?php echo $row['owner_lastname']?>"
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
                <div class="col-md-2 mb-3">
                  <label for="validationCustom021">M.I</label>
                  <input type="text" class="form-control" id="validationCustom021" name="middle_initial" placeholder="M.I" value="<?php echo $row['owner_mi']?>"
                    required>
                  <div class="valid-feedback">
                    Looks good!
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-12 mb-3">
                  <label for="validationCustomUsername">Username</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend">@</span>
                    </div>
                    <input type="text" class="form-control" id="validationCustomUsername" name="username" placeholder="Username" value="<?php echo $row['owner_username']?>"
                      aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                      Please choose a username.
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mb-3">
                  <label for="validationCustomPassword">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock"  aria-hidden="true"></i></span>
                    </div>
                    <input type="password" class="form-control" id="validationCustomPassword" name="password" placeholder="Password" value="<?php echo $row['owner_password']?>"
                      aria-describedby="inputGroupPrepend" required>
                    <div class="invalid-feedback">
                      Please choose a password.
                    </div>
                  </div>
                </div>

                <div class="col-md-12 mb-3">
                  <label for="recheckValidationCustomPassword">Confirm Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    </div>
                    <input type="password" class="form-control" id="recheckValidationCustomPassword" placeholder="Password"  value="<?php echo $row['owner_password']?>"
                      aria-describedby="inputGroupPrepend" name="confirm_password" id="recheckValidationCustomPassword" required>
                    </div>
                    <label  id="message"></label>
                  </div>
                  
              </div>
              <input class="btn btn-primary" type="submit" value="Update" name="updateOwner">
              <a class="btn btn-danger"  href="index.php">Cancel</a>
            </form> 
    </div>
  </div>




</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
<script src="assets/js/passwordValidation.js"></script>
<?php

} else {
  header("Location:../index.php?Please_login");
}
?>
</html>
