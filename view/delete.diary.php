<?php
require_once '../model/diary.model.php';
require_once '../model/owner.model.php';

if ($_SESSION) {
$Diary=new Diary();
$Owner=new Owner();
$owner=$Owner->getOwnerById($_SESSION['owner_id']);
$diary=$Diary->getDiaryById($_GET['id']);
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>MyDiary</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/custom.style.css" rel="stylesheet">
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><i class="fa fa-book" aria-hidden="true"></i>MyDiary</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?php echo $owner['owner_lastname'].",".$owner['owner_firstname']?>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="settings.php">Settings</a>
            <div class="dropdown-divider"></div>
            <form action="../controller/owner/owner.log.php" method="post">
              <input class="dropdown-item" type="submit" value="Logout" name="logout">
            </form>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="diary.php">Diary <span class="sr-only">(current)</span></a>
        </li>

      </ul>

    </div>
  </nav>
  <div class="custom-width">
    <div class="container custom-padding">
      <h1 class="display-4">Delete Diary</h1>
      <p>Do you want to delete <em><?php echo $diary['diary_label'];?></em> ?</p>
      <form class="needs-validation" novalidate method="POST" action="../controller/diary/diary.controller.php?id=<?php echo $diary['diary_id'];?>">
            <input class="btn btn-warning"type="submit" value="Delete" name="deleteDiary">
            <a  class="btn btn-danger" href="diary.php">Cancel</a>

      </form>
    </div>
  </div>




</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
<?php

} else {
  header("Location:../index.php?Please_login");
}
?>

</html> 