<?php
require_once '../model/story.model.php';
require_once '../model/owner.model.php';

if ($_SESSION) {
$Story=new Story();
$Owner=new Owner();
$owner=$Owner->getOwnerById($_SESSION['owner_id']);
$stories=$Story->getAllStory();
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>MyDiary</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/custom.style.css" rel="stylesheet">
  <link href="assets/css/custom.css" rel="stylesheet">
  <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><i class="fa fa-book" aria-hidden="true"></i>MyDiary</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?php echo $owner['owner_lastname'].",".$owner['owner_firstname']?>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="settings.php">Settings</a>
            <div class="dropdown-divider"></div>
            <form action="../controller/owner/owner.log.php" method="post">
              <input class="dropdown-item" type="submit" value="Logout" name="logout">
            </form>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="diary.php">Diary <span class="sr-only">(current)</span></a>
        </li>
      </ul>

    </div>
  </nav>

    <div class="custom-width">
    <div class="container custom-padding">
    <div class="form-row">
            <div class="col-md-4 mb-3">
            <form action="#" method="POST">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-tag" aria-hidden="true"></i></span>
                  </div>
                    <input    class="form-control" type="date" name="startDate" id="validationCustomLabel" require>
                  <div class="invalid-feedback">
                    Please input a Story Title.
                  </div>
                </div>
              </div>
              <div class="col-md-1 mb-3">
              <div class="input-group">
              <div class="input-group-prepend">
              </div>
                <label class="form-control" readonly>to</label>
            </div>
              </div>
          <div class="col-md-4 mb-3">
 
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
              </div>

                <input type="date"  class="form-control" name="endDate" id="validationCustomDate" require>
              <div class="invalid-feedback">
                Please choose a Date.
              </div>
            </div>
          </div>
          <div class="col-md-2 mb-3">
 
 <div class="input-group">
   <div class="input-group-prepend">
     <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
   </div>

        <input class="btn btn-success" type="submit" value="Filter" name="filter">

 </div>
 </form>
</div>

        </div> <!-- end form-row-->
    </div>
  </div>
<?php
if(isset($_POST['filter'])){
  $story=new Story();
  $startDate=$_POST['startDate'];
  $endDate=$_POST['endDate'];
  $data=array($startDate,$endDate);
  $filterResult= $story->filterStory(array($_SESSION['owner_id']),$startDate,$endDate);
  if(count($filterResult)==0){
    echo "<script> alert('NO stories available')</script>";
  }
?>

  <?php
  foreach ($filterResult as $story) {
    ?>
  <div class="custom-width">
    <div class="container custom-padding">
    <div class="inline">
        <?php
        if ($_SESSION['owner_id'] == $story['owner_id']) {
          ?>
        <a href="edit.story.php?id=<?php echo $story['story_id'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <a href="delete.story.php?id=<?php echo $story['story_id'] ?>"><i class="fa fa-times" aria-hidden="true"></i></a>

        <?php
      }
      ?>
      </div>
      <h1 class="display-4">
        <?php echo $story['story_title'] ?>
      </h1>
      <p class="lead"><?php echo $story['story_content'] ?></p>
      <?php echo $story['story_date'] ?>
    </div>
  </div>
  <?php
 }
}
?>


<?php

?>



</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
<?php

} else {
  header("Location:../index.php?Please_login");
}
?>

</html>