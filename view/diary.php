<?php
require_once '../model/diary.model.php';
require_once '../model/owner.model.php';

if ($_SESSION) {
$Diary=new Diary();
$Owner=new Owner();
$owner=$Owner->getOwnerById($_SESSION['owner_id']);
$diaries=$Diary->getDiaryByOwner($_SESSION['owner_id']);
  ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>MyDiary</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/custom.style.css" rel="stylesheet">
  <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/css/custom.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php"><i class="fa fa-book" aria-hidden="true"></i>MyDiary</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <?php echo $owner['owner_lastname'].",".$owner['owner_firstname']?>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="settings.php">Settings</a>
            <div class="dropdown-divider"></div>
            <form action="../controller/owner/owner.log.php" method="post">
              <input class="dropdown-item" type="submit" value="Logout" name="logout">
            </form>
          </div>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item active">
          <a class="nav-link" href="diary.php">Diary <span class="sr-only">(current)</span></a>
        </li>
      </ul>

    </div>
  </nav>
  <div class="custom-width">
    <div class="container custom-padding">
      <h1 class="display-4">Create Diary</h1>
      <form class="needs-validation" novalidate method="POST" action="../controller/diary/diary.controller.php">
        <div class="form-row">
          <div class="col-md-12 mb-3">
            <!-- <label for="validationCustomDate">Username</label> -->
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-calendar" aria-hidden="true"></i></span>
              </div>
              <input type="date" class="form-control" id="validationCustomDate" 
                aria-describedby="inputGroupPrepend" name="date" required>
              <div class="invalid-feedback">
                Please choose a Date.
              </div>
            </div>
          </div>
          <div class="col-md-12 mb-3">
              <!-- <label for="validationCustomLabel">Diary Label</label> -->
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-tag" aria-hidden="true"></i></span>
                </div>
                <input type="text" class="form-control" id="validationCustomLabel" placeholder="Diary Label"
                  aria-describedby="inputGroupPrepend" name="label" required>
                <div class="invalid-feedback">
                  Please input a Label.
                </div>
              </div>
            </div>
            <div class="col-md-12 mb-3">
                <!-- <label for="validationCustomLabel">Diary Label</label> -->
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-check-square" aria-hidden="true"></i></span>
                  </div>

                  <!-- <input type="text" class="form-control" id="validationCustomLabel" placeholder="Diary Label"
                    aria-describedby="inputGroupPrepend" name="label" required> -->
                  <select  class="form-control" name="status" id="validationCustomLabel">
                      <option value="1">Active</option>
                      <option value="2">Forgotten</option>
                  </select>
                  <div class="invalid-feedback">
                    Please input a Status.
                  </div>
                </div>
              </div>
        </div>
        <input class="btn btn-primary" type="submit" value="Create" name="createDiary">
      </form>
    </div>
  </div>
  <?php
foreach($diaries as $dia){
?>
  <div class="custom-width">
    <div class="container custom-padding">
    <div class="inline">
        <a href="edit.diary.php?id=<?php  echo $dia['diary_id']?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
        <?php
          if($dia['diary_status']==1){
        ?>
        <a href="delete.diary.php?id=<?php  echo $dia['diary_id']?>"><i class="fa fa-times" aria-hidden="true"></i></a>
        <a href="storylist.php?id=<?php  echo $dia['diary_id']?>?&owner_id=<?php  echo $dia['owner_id']?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
        <?php
          }
?>
      </div>
      <h1 class="display-4">
        <?php  echo $dia['diary_label']?>
      </h1>
      <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
      <?php echo $dia['diary_datecreated']?>
    </div>
  </div>
  <?php
}
?>



</body>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/validate.js"></script>
<?php

} else {
  header("Location:../index.php?Please_login");
}
?>

</html>