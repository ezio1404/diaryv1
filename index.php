<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>MyDiary</title>
    <link href="view/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="view/assets/css/custom.style.css" rel="stylesheet">
    <link href="view/assets/css/custom.css" rel="stylesheet">
    <link href="view/assets/fonts/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <div id="login">
        <div class="container ">
            <h3 class="text-center text-primary pt-5"></h3>
            <div id="login-row" class="row justify-content-center align-items-center custom-width">
                <div id="login-column" class="col-md-6">
                    <div id="login-box" class="col-md-12">
                        <h3 class="text-center text-info">Login</h3>
                        <form class="needs-validation" novalidate method="POST" action="controller/owner/owner.log.php">

                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustomUsername">Username</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend">@</span>
                                        </div>
                                        <input type="text" class="form-control" id="validationCustomUsername"
                                            placeholder="Username" aria-describedby="inputGroupPrepend" name="username" required>
                                        <div class="invalid-feedback">
                                            Please choose a username.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustomPassword">Password</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock"
                                                    aria-hidden="true"></i></span>
                                        </div>
                                        <input type="password" class="form-control" id="validationCustomPassword"
                                            placeholder="Password" aria-describedby="inputGroupPrepend" name="password" required>
                                        <div class="invalid-feedback">
                                            Please input a password.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                               <p>Don't have an Account?<a href="register.php"><em>Register here!</em></a></p>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Login" name="login">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="view/assets/js/jquery.min.js"></script>
<script src="view/assets/js/bootstrap.min.js"></script>
<script src="view/assets/js/validate.js"></script>
<script src="view/assets/js/passwordValidation.js"></script>
</body>

</html>